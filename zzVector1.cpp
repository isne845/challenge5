#include <iostream>
#include <random>
#include <vector>
#include <algorithm>
#include <time.h>
using namespace std;



int vSize(vector<int> v){ //O(1) the value of v.size() is stored in the vector.
    return v.size();
}

//Sum of random number we got
int vSum(vector<int> v){ //O(n)
    int sum = 0;
    for(int i = 0 ; i < v.size(); ++i){
        sum += v[i];
    }
    return sum;
}


//Highest Value
int vHighestValue(vector<int> v){ //O(n)
    int highestValue = -1e9;
    for(int i = 0 ; i < v.size(); ++i){
        if(v[i] > highestValue){
            highestValue = v[i];
        }
    }
    return highestValue;
}


//Lowesst number in size of vector 
int vLowestValue(vector<int> v){ //O(n)
    int lowestValue = 1e9;
    for(int i = 0 ; i < v.size(); ++i){
        if(v[i] < lowestValue){
            lowestValue = v[i];
        }
    }
    return lowestValue;
}

double vMean(vector<int> v){ //O(n) + O(n) = O(n)
    return ((double)vSum(v))/vSize(v);
}

double vMedian(vector<int> v){ //O(1) the value of v.size() is stored in the vector.
    if(vSize(v) % 2 == 0){
        return ((double)v[vSize(v)/2] + (double)v[(vSize(v)/2)-1])/2;
    }
    return v[vSize(v)/2];
}


//Please specify what to return if there is two MODE value
double vMode(vector<int> v){ //O(n) + O(n) + O(n) = O(n)
    vector<int> count(101,0);

    for(int i = 0 ; i < v.size(); ++i){
        count[v[i]]++;
    }
    
    int highestCount = vHighestValue(count);

    for(int i = 0 ; i < v.size(); ++i){
        if(count[v[i]] == highestCount){
            return v[i];
        }
    }
    return -1;
}

int vRange(vector<int> v){ //O(n) + O(n) = O(n)
    return vHighestValue(v) - vLowestValue(v);
}

int vEvenCount(vector<int> v){ //O(n)
    int evenCount = 0;
    for(int i = 0 ; i < v.size(); ++i){
        if(v[i] % 2 == 0){
            evenCount++;
        }
    }
    return evenCount;
}

int vOddCount(vector<int> v){ //O(n)
    return vSize(v) - vEvenCount(v);
}

void vDisplaySorted(vector<int> v){  //O(n log n) + O(n)
    sort(v.begin(),v.end());

    for(int i = 0 ; i < v.size(); ++i){
        cout << v[i] << " ";
    }cout << endl;

    return;
}

//Main Function
int main(){

    random_device rd;
    default_random_engine eng{static_cast<long unsigned int>(time(NULL))};
    uniform_int_distribution<> dist(0, 1e9);
    srand(time(NULL));

    int N = 50 + dist(eng)%101;
    cout << N << endl;
    vector<int> vec;

    for(int i = 0 ; i < N; ++i){
        int a = dist(eng)%101;
        vec.push_back(a);
        cout << a << " ";
    }cout << endl;


    cout << vSize(vec) << endl;
    cout << vSum(vec) << endl;
    cout << vHighestValue(vec) << endl;
    cout << vLowestValue(vec) << endl;
    cout << vMean(vec) << endl;
    cout << vMedian(vec) << endl;
    cout << vMode(vec) << endl;
    cout << vRange(vec) << endl;
    cout << vEvenCount(vec) << endl;
    cout << vOddCount(vec) << endl;
    vDisplaySorted(vec);





    return 0;
}
